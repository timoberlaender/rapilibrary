## GroR

## Installation

The GroR package is not added to any public repository and must therefore be installed by hand. This can be done in different ways depending on your needs. 

### adding the R file as a Source

It is possible to download the GroR.R file from this repository and link it to your R file with the code below:

```R
source("path/to/GroR.R")
...
```

This line replaces `library(GroR)`

### Package

The releases contain packages that can be installed directly with either the R command:
```R
install.packages("path/to/GroR_x.x.x.tar.gz", repos = NULL, type = "source")
```

Alternatively, it is possible to install the package with the R Studio package manager by selecting "install from package archive file".


## Structure

The library is separated into two sets of functions, GroLink functions for application requests and WBRef functions for workench requests. 


### GroLink
GroLink functions take the URL of the API as an argument, this URL is defined as follows:

`http://<ip-address>:<port>/api/`

- The IP address is either localhost or the address of the device running GroIMP
- The port is defined by the start of the GroIMP API, by default it is: 58081  

Therefore it is likely that your URL is: `http://localhost:58081/api`



The available functions for GroLink are:

| function | return | description |
| ---      | ---    |  ---        |
|GroLink.create(url)| wb | creates a new workbench of the emptyRGG template|
|GroLink.create(url,type) | wb | create a new Workbench of the given template |
|GroLink.open(url,path=path) | wb | opens the project at the given path |
|GroLink.open(url,content=binary) | wb | opens the proivded file |


### WBRef
each WBRef function takes a workbench reference (wb) as a parameter to clarify on which workbench the command ist supposed to be executed.


| function | return | description |
| ---      | ---    |  ---        |
| WBRef.listRGGFunctions(wb) | json | list available RGG functions| 
| WBRef.runRGGFunction(wb,name) | json | execute the given RGG function |
| WBRef.runXLQuery(wb,query) | json | execute the given xl query |
| WBRef.compile(wb) | json | compile |
| WBRef.listFiles(wb) | json | list all source files |
| WBRef.renameFile(wb, name, newName) | json | rename a file |
| WBRef.addFile(wb, path) | json | add file form the given path |
| WBRef.addFile(wb, name, content)| json | add the content as a new file with the given name|
| WBRef.updateFile(wb,name,content)| json | set the content as content of the fiel of the given name |
| WBRef.removeFile(wb,name) | json | remove the given file with the given name|
| WBRef.getFile(wb, name) | String | returns the content of the given file |
| WBRef.getProjectGraph(wb) | String | returns the project graph as string [1] |
| WBRef.addNode(wb, path) | json | adds the file given by the path to the scene.(e.g. obj or xeg) |
| WBRef.addNode(wb,extension,content) | json | adds the content as an fiel of the tye of the extension to the scene. |
| WBRef.export3d(wb,extension) | string | returns the content of a file of the given extension exported form the 3d scene | 
| WBRef.export3d_toFile(wb,path) | stirng | exports the 3d scene to the given path | 
| WBRef.listDatasets(wb) | json | list the existing datasets of the model | 
| WBRef.getDataset(wb, name) | string | returns the content of a datasets |
| WBRef.save(wb) | string | returns the project as a decoded gsz file | 
| WBRef.save(wb,path) | json | saves the project to the given path | 


\[1]: the JSON parser included in the library is quite limited and for the ProjectGraph an external one is more suitable, therefore the getProjectGraph function returns a string even though the content is JSON. 

## Usage



```R
library(dplyr)
library(httr2)
library(GroR)
wb<-GroLink.create("http://localhost:58081/api")

ls <-WBRef.listFiles(wb)
ls

content <- WBRef.getFile(wb,"Model.rgg")
content

text <-"
public void run ()
  {print(\"hallooo\");
}"

WBRef.updateFile(wb,"Model.rgg",text)
WBRef.compile(wb)
result <-WBRef.runRGGFunction(wb,"run")

paste(result$console,sep="\n")



```